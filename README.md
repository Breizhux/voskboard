# VoskBoard

**A very simple application that allows you to speak, and the text is written for you in live !**

The application pretends to be a keyboard, so no compatibility worries, whatever the application, the transcription will work!

The application is based on the [Vosk project](https://alphacephei.com/en/).

If you want to transcribe file with very high fidelity, see my [whisper gui project](https://gitlab.com/Breizhux/faster-whisper-gui)!

![Capture of voskboard gui](./capture.png)

## Usage

**Very simple !
Use the icon to start transcription !**

Or by command :

```bash
voskboard
```

## Installation instruction

On debian-like system (tested on debian 11 and Linux Mint 21) :

```bash
git clone https://gitlab.com/Breizhux/voskboard.git
cd voskboard
sudo apt install virtualenv python3-gi #if needed
./build.sh
sudo apt install ./voskboard.deb
```

To uninstall :

```bash
sudo apt remove --purge --auto-remove voskboard
```

## Description

The project is based on Vosk. The advantage is that it is a **lightweight speech-to-text** project that **runs in real time**, **supporting a large number of languages**.
The disadvantage is that the very light models make a **few mistakes**. But above all, **punctuation is not yet present**. It is currently being added, but will in all cases require offline processing;)

To work, the interface lets you choose a large number of options. When the transcript is launched, the model is loaded, which can take a little time for fairly complete models. However, when paused, the model is not unloaded, allowing you to restart the transcription immediately.

Then, various output are proposed to you: output in keyboard mode, in file mode (what is heard is directly written in a file, and output in text area mode, directly integrated into the voskboard interface.

You can easily manage all the templates using the "configuration" tab. The templates are saved in `~/. config/voskboard/`.
