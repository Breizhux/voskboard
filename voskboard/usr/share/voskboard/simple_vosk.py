#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
import sys
import ast
import queue
from time import sleep, time #TODO : remove time, just for debug
import sounddevice as sd
from threading import Thread
from vosk import Model, KaldiRecognizer
from pynput.keyboard import Key, Controller



class VoskModel :
    """ Simple Vosk Model."""
    def __init__(self, **kwargs) :
        if "model_path" in kwargs :
            self.model_path = kwargs['model_path']
            self.model = Model(model_path=self.model_path)
        else :
            self.model_path = None
            self.model = None

    def load(self, model_path) :
        if not os.path.exists(model_path) :
            sys.stderr.write(f"Error : \"{model_path}\" does not exists.")
            sys.stderr.flush()
            sys.exit(1)
        self.model_path = model_path
        del self.model
        self.model = Model(model_path=self.model_path)



class SimpleVosk(Thread) :
    """ A simple to use Vosk interface."""
    options = {
        "samplerate" : 44100,
        "blocksize"  : 8000,
        "device"     : None,
        "dtype"      : "int16",
        "channels"   : 1
    }
    def __init__(self, model, **kwargs) :
        Thread.__init__(self)
        #Set options
        for i in kwargs :
            if i in self.options :
                self.options[i] = kwargs[i]
        #Set model
        self.__model = model
        #Set need tools
        self.__queue = queue.Queue()
        self.__lock = True
        self.__filter = "all"
        self.__post_process = self.__self_output #function called after recognized sentence
        self.__buffer = [] #buffer of recognized sentence
        self.__sentences_history = []

    def run(self) :
        """ Start Vosk recognition and print it."""
        #start recognition
        sys.stdout.write("[start] Recognizing text...\n")
        sys.stdout.flush()
        latest_partial_sentence = ""
        #live stream
        with sd.RawInputStream( samplerate=self.options['samplerate'],
                                blocksize=self.options['samplerate'],
                                device=self.options['device'],
                                dtype=self.options['dtype'],
                                channels=self.options['channels'],
                                callback=self.__callback ) :
            rec = KaldiRecognizer(self.__model.model, self.options['samplerate'])
            while self.__lock :
                try : data = self.__queue.get(timeout=1)
                except : continue
                #validate sentence : {"text" : ...}
                if rec.AcceptWaveform(data) :
                    result = rec.Result()
                    #filter empty sentence
                    if result == "" : continue
                    self.__sentences_history.append(result)
                    self.__post_process(ast.literal_eval(result))
                #partial sentence : {"partial" : ...}
                elif self.__filter != "text" :
                    result = rec.PartialResult()
                    #filter empty sentence
                    if result == "" : continue
                    elif result == latest_partial_sentence : continue
                    latest_partial_sentence = result #keep current sentence
                    self.__post_process(ast.literal_eval(result))
        #stop recognition
        self.__lock = True
        sys.stdout.write("[stop] Recognition is stop...\n")
        sys.stdout.flush()

    def stop(self) :
        #lock the loop
        self.__lock = False
        #wait end of loop
        while not self.__lock : sleep(0.01)

    def __iter__(self) :
        """ Allows you to iterate sentence directly."""
        while self.__lock :
            #wait for new vosk sentence
            while len(self.__buffer) == 0 and self.__lock : sleep(0.1)
            yield self.__buffer.pop(0)

    def __callback(self, indata, frames, time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:
            print(status, file=sys.stderr)
        self.__queue.put(bytes(indata))

    def __self_output(self, sentence) :
        """ The function to self store recognized sentence.
        You can iter object to get these sentences."""
        self.__buffer.append(sentence)

    def set_filter(self, f="all") :
        """ Set what results returning when recognition is start.
        Available : all (partial and text)
                    text (only complete sentence)
        """
        if not f in ["all", "text"] :
            sys.stderr.write(f"Error : {f} is not available for filter.")
            sys.stderr.flush()
            return None
        self.__filter = f

    def set_post_process(self, f=None) :
        """ Set the function to store recognized sentence.
        By default, store in local buffer available from for loop.
        The function need to be a non-blocking function !"""
        if f is None : f = self.__self_output
        self.__post_process = f



class VoskKeyboard(Thread) :
    def __init__(self) :
        Thread.__init__(self)
        self.__buffer = []
        self.__last_sentence = ""
        self.__last_char = " " #for first loop, set to " ", after, default is ""
        self.__lock = True
        self.__keyboard = Controller()

    def run(self) :
        while self.__lock :
            #wait for new vosk sentence
            while len(self.__buffer) == 0 and self.__lock : sleep(0.1)
            for i in self.__buffer :
                if len(i) == 0 : continue
                #get sentence, and character to type sequence
                s_type, sentence = i.popitem()
                if sentence == "" : continue
                sequence = self.__diff_sentence(sentence)
                #adjust last char in relation to correction
                if sequence[0] != 0 :
                    if sequence[0] != len(self.__last_sentence) :
                        self.__last_char = self.__last_sentence[(sequence[0]+1)*-1]
                #type chars
                self.__type(sequence)
                #if this is new sequence
                if s_type == "text" :
                    self.__last_sentence = ""
                #else remember necessary
                else :
                    self.__last_sentence = sentence
                self.__last_char = ""
        #it's ok ! I'm finish !
        self.__lock = True

    def stop(self) :
        #lock the loop
        self.__lock = False
        #wait end of loop
        while not self.__lock : sleep(0.01)

    def __diff_sentence(self, right) :
        """ Calculate the new sequence of letters in relation
        to the last one. Return (number of char to remove, new chars)."""
        #find first different character
        index = 0
        for i, j in zip(self.__last_sentence, right) :
            if i != j :
                char_to_remove = len(self.__last_sentence)-index
                char_to_type = right[index:]
                return (char_to_remove, char_to_type, )
            index += 1
        #if chain equal
        if len(self.__last_sentence) == len(right) :
            return (0, "")
        #if second chain just great than first
        elif len(self.__last_sentence) < len(right) :
            return (0, right[index:])
        #if first chain just great than second : normally impossible...
        elif len(self.__last_sentence) > len(right) :
            return (len(self.__last_sentence) - len(right), "")

    def __type(self, datas) :
        """ Emulate keyboard, and type characters.
        Input : (number of char to remove, new chars)."""
        #remove number of char
        for i in range(datas[0]) :
            self.__keyboard.press(Key.backspace)
            self.__keyboard.release(Key.backspace)
        #if no data to type, stop
        if len(datas[1]) == 0 : return None
        #if need to add a space
        if self.__last_char != " " and datas[1][0] != " " :
            self.__keyboard.type(" ")
        #type string chars
        self.__keyboard.type(datas[1])

    def input_sentence(self, sentence) :
        self.__buffer.append(sentence)



class VoskFile :
    def __init__(self, path, printing=None) :
        self.path = path
        if not os.path.exists(self.path) :
            with open(self.path, 'w') as file : pass
        #possibility of label to show recognized in real time : set the function to call to change the label, not just the label
        self.__printing = printing
        self.__lock = True
        self.__last_char = ""

    def __print(self, text) :
        if self.__printing is None : return None
        self.__printing(text)

    def write(self, text) :
        if "partial" in text :
            self.__print(text['partial'])
            return None
        text = text['text']
        if text == "" : return None
        with open(self.path, 'a') as file :
            print(text)
            if text[0] != " " and self.__last_char != " " :
                file.write(" ")
            file.write(text)
        self.__last_char = text[-1]



#def my_callback(sentence) :
#    print(sentence)


if __name__ == "__main__" :
    ## Init vosk model, engine and keyboard
    vosk_model = VoskModel(
        model_path="/home/user/.config/voskboard/models/vosk-model-small-fr-0.22"
    )
    vosk_engine = SimpleVosk(vosk_model)
    vosk_keyboard = VoskKeyboard()

    ## Setting up engine
    #vosk_engine.set_filter("all")
    vosk_engine.set_post_process(f=vosk_keyboard.input_sentence)

    ## Start element
    vosk_engine.start()
    vosk_keyboard.start()

    sleep(10)

    ## Print result !
    #for loop to display 50 transcipted message
    #sentence_number_before_stop = 50
    #index = 0
    #for i in vosk_engine :
    #    index += 1
    #    if index == sentence_number_before_stop : break
    #    print(i)


    ## Stop simple vosk engine
    vosk_engine.stop()
    vosk_keyboard.stop()
