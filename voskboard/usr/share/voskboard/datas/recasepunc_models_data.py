RECASEPUNC_MODEL = [
    {
        "name" : "vosk-recasepunc-en-0.22",
        "human" : "Anglais",
        "language" : ["en-us"], #key word contained in name of vosk model
        "size" : "1.6G",
        "url" : "https://alphacephei.com/vosk/models/vosk-recasepunc-en-0.22.zip",
    },
    {
        "name" : "recasepunc-fr",
        "human" : "Français",
        "language" : ["fr"],
        "size" : "1.5G",
        "url" : "https://github.com/benob/recasepunc/releases/download/0.3/fr.22000",
    },
    {
        "name" : "vosk-recasepunc-ru-0.22",
        "human" : "Russe",
        "language" : ["ru"],
        "size" : "1.6G",
        "url" : "https://alphacephei.com/vosk/models/vosk-recasepunc-ru-0.22.zip",
    },
    {
        "name" : "vosk-recasepunc-de-0.21",
        "human" : "Allemand",
        "language" : ["de"],
        "size" : "1.1G",
        "url" : "https://alphacephei.com/vosk/models/vosk-recasepunc-de-0.21.zip",
    },
    {
        "name" : "recasepunc-it",
        "human" : "Italien",
        "language" : ["it"],
        "size" : "1.2G",
        "url" : "https://github.com/CoffeePerry/recasepunc/releases/download/v0.1.0/it.22000"
    },
    {
        "name" : "recasepunc-zh",
        "human" : "Chinois",
        "language" : ["zh"],
        "size" : "1.1G",
        "url" : "https://github.com/benob/recasepunc/releases/download/0.3/zh.24000"
    },
]

if __name__ == "__main__" :
    import json
    with open("recasepunc_models_data.json", 'w') as file :
        json.dump(RECASEPUNC_MODEL , file, indent=4)
