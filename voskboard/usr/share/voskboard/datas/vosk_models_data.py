VOSK_MODELS = [
    {
        "name" : "Ajouter votre propre modèle",
        "human" : "Autre modèle",
        "size" : "-",
        "url" : None
    },
    {
        "name" : "vosk-model-small-en-us-0.15",
        "human" : "Anglais - léger",
        "size" : "40M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.15.zip"
    },
    {
        "name" : "vosk-model-en-us-0.22",
        "human" : "Anglais",
        "size" : "1.8G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-en-us-0.22.zip",
    },
    {
        "name" : "vosk-model-fr-0.22",
        "human" : "Français",
        "size" : "1.4G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-fr-0.22.zip",
    },
    {
        "name" : "vosk-model-small-fr-0.22",
        "human" : "Français - léger",
        "size" : "41M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-fr-0.22.zip"
    },
    {
        "name" : "vosk-model-en-in-0.5",
        "human" : "Indien",
        "size" : "1G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-en-in-0.5.zip",
    },
    {
        "name" : "vosk-model-small-en-in-0.4",
        "human" : "Indien - léger",
        "size" : "36M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-en-in-0.4.zip"
    },
    {
        "name" : "vosk-model-cn-0.22",
        "human" : "Chinois",
        "size" : "1.3G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-cn-0.22.zip",
    },
    {
        "name" : "vosk-model-small-cn-0.22",
        "human" : "Chinois - léger",
        "size" : "42M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-cn-0.22.zip"
    },
    {
        "name" : "vosk-model-ru-0.22",
        "human" : "Russe",
        "size" : "1.5G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-ru-0.22.zip",
    },
    {
        "name" : "vosk-model-small-ru-0.22",
        "human" : "Russe - léger",
        "size" : "45M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-ru-0.22.zip"
    },
    {
        "name" : "vosk-model-de-0.21",
        "human" : "Allemand",
        "size" : "1.9G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-de-0.21.zip",
    },
    {
        "name" : "vosk-model-small-de-0.15",
        "human" : "Allemand - léger",
        "size" : "45M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-de-0.15.zip"
    },
    {
        "name" : "vosk-model-es-0.42",
        "human" : "Espagnol",
        "size" : "1.4G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-es-0.42.zip",
    },
    {
        "name" : "vosk-model-small-es-0.42",
        "human" : "Espagnol - léger",
        "size" : "39M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-es-0.42.zip"
    },
    {
        "name" : "vosk-model-pt-fb-v0.1.1-20220516_2113",
        "human" : "Portugais",
        "size" : "1.6G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-pt-fb-v0.1.1-20220516_2113.zip",
    },
    {
        "name" : "vosk-model-small-pt-0.3",
        "human" : "Portugais - léger",
        "size" : "31M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-pt-0.3.zip"
    },
    {
        "name" : "vosk-model-el-gr-0.7",
        "human" : "Grec",
        "size" : "1.1G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-el-gr-0.7.zip",
    },
    {
        "name" : "vosk-model-small-tr-0.3",
        "human" : "Turque - léger",
        "size" : "35M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-tr-0.3.zip"
    },
    {
        "name" : "vosk-model-vn-0.4",
        "human" : "Vietnamien",
        "size" : "78M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-vn-0.4.zip",
    },
    {
        "name" : "vosk-model-small-vn-0.4",
        "human" : "Vietnamien - léger",
        "size" : "32M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-vn-0.4.zip"
    },
    {
        "name" : "vosk-model-it-0.22",
        "human" : "Italien",
        "size" : "1.2G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-it-0.22.zip",
    },
    {
        "name" : "vosk-model-small-it-0.22",
        "human" : "Italien - léger",
        "size" : "48M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-it-0.22.zip"
    },
    {
        "name" : "vosk-model-small-ca-0.4",
        "human" : "Catalan - léger",
        "size" : "42M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-ca-0.4.zip",
    },
    {
        "name" : "vosk-model-ar-mgb2-0.4",
        "human" : "Arabe - léger",
        "size" : "300M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-ar-mgb2-0.4.zip"
    },
    {
        "name" : "vosk-model-ar-0.22-linto-1.1.0",
        "human" : "Arabe",
        "size" : "300M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-ar-0.22-linto-1.1.0.zip"
    },
    {
        "name" : "vosk-model-fa-0.5",
        "human" : "Farsi",
        "size" : "1G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-fa-0.5.zip"
    },
    {
        "name" : "vosk-model-small-fa-0.5",
        "human" : "Farsi - léger",
        "size" : "60M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-fa-0.5.zip"
    },
    {
        "name" : "vosk-model-tl-ph-generic-0.6",
        "human" : "Filipino",
        "size" : "320M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-tl-ph-generic-0.6.zip"
    },
    {
        "name" : "vosk-model-uk-v3",
        "human" : "Ukrainien",
        "size" : "343M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-uk-v3.zip"
    },
    {
        "name" : "vosk-model-small-uk-v3-nano",
        "human" : "Ukrainien - léger",
        "size" : "73M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-uk-v3-nano.zip"
    },
    {
        "name" : "vosk-model-kz-0.15",
        "human" : "Kazakh",
        "size" : "378M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-kz-0.15.zip"
    },
    {
        "name" : "vosk-model-small-kz-0.15",
        "human" : "Kazakh - léger",
        "size" : "42M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-kz-0.15.zip"
    },
    {
        "name" : "vosk-model-small-sv-rhasspy-0.15",
        "human" : "Suédois",
        "size" : "289M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-sv-rhasspy-0.15.zip"
    },
    {
        "name" : "vosk-model-ja-0.22",
        "human" : "Japonais",
        "size" : "1G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-ja-0.22.zip"
    },
    {
        "name" : "vosk-model-small-ja-0.22",
        "human" : "Japonais - léger",
        "size" : "48M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-ja-0.22.zip"
    },
    {
        "name" : "vosk-model-small-eo-0.42",
        "human" : "Esperanto - léger",
        "size" : "42M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-eo-0.42.zip"
    },
    {
        "name" : "vosk-model-hi-0.22",
        "human" : "Hindi",
        "size" : "1.5G",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-hi-0.22.zip"
    },
    {
        "name" : "vosk-model-small-hi-0.22",
        "human" : "Hindi - léger",
        "size" : "42M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-hi-0.22.zip"
    },
    {
        "name" : "vosk-model-small-cs-0.4-rhasspy",
        "human" : "Czech - léger",
        "size" : "44M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-cs-0.4-rhasspy.zip"
    },
    {
        "name" : "vosk-model-small-pl-0.22",
        "human" : "Polonais - léger",
        "size" : "50M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-pl-0.22.zip"
    },
    {
        "name" : "vosk-model-small-uz-0.22",
        "human" : "Uzbek - léger",
        "size" : "49M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-uz-0.22.zip"
    },
    {
        "name" : "vosk-model-small-ko-0.22",
        "human" : "Coréen - léger",
        "size" : "82M",
        "url" : "https://alphacephei.com/vosk/models/vosk-model-small-ko-0.22.zip"
    },
]

if __name__ == "__main__" :
    import json
    with open("vosk_models_data.json", 'w') as file :
        json.dump(VOSK_MODELS , file, indent=4)
