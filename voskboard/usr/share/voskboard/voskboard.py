#!/usr/bin/env python3

import pyperclip
import simple_vosk
import model_manager
from time import sleep
from threading import Thread
from subprocess import Popen, PIPE

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gtk
from gi.repository.Gdk import SELECTION_CLIPBOARD



class ProgressDialog(Thread) :
    """ Simple zenity pulsate progress bar."""
    def __init__(self, title, text) :
        Thread.__init__(self)
        self.title = title
        self.text = text
        self.command = "zenity --progress --pulsate --width 300 --no-cancel --auto-close"
        self.command += f" --auto-kill --title=\"{self.title}\" --text=\"{self.text}\""
        self.process = None

    def run(self) :
        self.process = Popen(self.command,
            shell=True, encoding='utf-8', executable="/bin/bash",
            stdout=PIPE, stdin=PIPE, stderr=PIPE)

    def stop(self) :
        self.write("100\n")

    def write(self, text) :
        self.process.stdin.write(text)
        self.process.stdin.flush()

    def read(self, output="stdout") :
        if output == "stdout" :
            return self.process.stdout.read()
        else :
            return self.process.stderr.read()

    def is_alive(self) :
        return self.process.poll() is None

    def get_output(self) :
        return (self.process.poll(), self.read())

    def set_progress(self, number) :
        if isinstance(number, float) :
            number = int(number*100)
        self.write(f"{number}\n")

    def set_label(self, text) :
        self.write(f"# {text}\n")



class VoskBoard() :
    messages = {
        "load_model" : "Chargement du modèle...",
        "error_outfile" : "Erreur, vous devez indiquer le fichier de sortie.",
        "error_infile" : "Erreur, vous devez indiquer le fichier source.",
        "error_no_model" : "Erreur, vous devez sélectionner un modèle (une langue).",
    }
    def __init__(self) :
        self.vosk_model = None #model use in engine
        self.vosk_engine = None
        self.vosk_models = model_manager.ModelManager("vosk")
        self.vosk_output = None #method of output for vosk_engine
        #interface
        self.interface = Gtk.Builder()
        self.interface.add_from_file("./voskboard.glade")
        #set content of interface
        self.__set_available_lang()
        #show interface
        self.window = self.interface.get_object("window")
        self.window.connect("delete-event", self.cancel)
        self.window.show()
        #connect interface to self
        self.interface.connect_signals(self)

    def cancel(self, *args) :
        if self.vosk_output is not None : #if currently recognize from mic
            self.stop_transcription()
        Gtk.main_quit()

    def __getitem__(self, name) :
        """ Return the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.Entry) :
            return key.get_text()
        elif isinstance(key, Gtk.TextBuffer) :
            s, e = key.get_bounds()
            return key.get_text(s, e, False)
        elif isinstance(key, Gtk.FileChooserButton) :
            return key.get_filename()
        elif isinstance(key, Gtk.ComboBoxText) :
            return key.get_active()
        elif isinstance(key, Gtk.CheckButton) :
            return key.get_active()
        elif isinstance(key, Gtk.Button) :
            key.get_visible()
        elif isinstance(key, Gtk.ListBox) :
            return key.get_selected_row().get_child().get_label()
        else :
            print(f"Unknow get fonction for type \"{type(key)}\" : {name}")

    def __setitem__(self, name, value) :
        """ Set the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.Label) :
            key.set_label(value)
        elif isinstance(key, Gtk.FileChooserButton) :
            key.set_filename(value)
        elif isinstance(key, Gtk.ListStore) :
            for i in value : key.append([i,])
        elif isinstance(key, Gtk.ComboBoxText) :
            key.remove_all()
            for i in value : key.append_text(i)
            key.set_active(0)
        elif isinstance(key, Gtk.CheckButton) :
            return key.set_active(value)
        elif isinstance(key, Gtk.ListBox) :
            for i in key.get_children() : key.remove(i)
            for i in value :
                label = Gtk.Label()
                label.set_text(i)
                label.set_alignment(0,0)
                label.show()
                key.add(label)
        elif isinstance(key, Gtk.TextBuffer) :
            return key.set_text(value)#, len(value))
        else :
            print(f"Unknow set fonction for type \"{type(key)}\"")

    def __widget_hide(self, name, value) :
        """ Hide or show a widget by name."""
        key = self.interface.get_object(name)
        key.set_visible(value)

    def __set_available_lang(self) :
        available_lang = list(map(lambda x: x['human'], self.vosk_models.get_available()))
        self['vosk_model'] = available_lang
        self['vosk_model_list'] = available_lang

    def __set_window_priority(self, priority=1) :
        """ Set windows always on top.
        0 = always on top, 1 = normal, 2 = minimize window"""
        if priority == 0 :
            self.window.set_keep_above(True)
        elif priority == 1 :
            self.window.set_keep_above(False)
        elif priority == 2 :
            self.window.iconify()

    def set_vosk_widget(self, *args) :
        """ Set the required output widgets.
        0 : keyboard, 1 : file, 2 : self textview."""
        # Set for output
        if self['vosk_output'] == 0 : #case output is keyboard
            self.__widget_hide("vosk_textview_label", False)
            self.__widget_hide("vosk_textview", False)
            self.__widget_hide("vosk_file_target_label", False)
            self.__widget_hide("vosk_file_target", False)
        elif self['vosk_output'] == 1 : #case output is file
            self.__widget_hide("vosk_textview_label", False)
            self.__widget_hide("vosk_textview", False)
            self.__widget_hide("vosk_file_target_label", True)
            self.__widget_hide("vosk_file_target", True)
        elif self['vosk_output'] == 2 : #case output is textview
            self.__widget_hide("vosk_textview_label", True)
            self.__widget_hide("vosk_textview", True)
            self.__widget_hide("vosk_file_target_label", False)
            self.__widget_hide("vosk_file_target", False)

    def __set_vosk_model(self, model_path) :
        """ If current model is not the model of user choice, load it."""
        if self.vosk_model is None :
            progress_bar = ProgressDialog("Chargement du modèle", "Chargement du modèle en cours...")
            progress_bar.start()
            self.vosk_model = simple_vosk.VoskModel(model_path=model_path)
        elif self.vosk_model.model_path != model_path :
            progress_bar = ProgressDialog("Chargement du modèle", "Chargement du modèle en cours...")
            progress_bar.start()
            self.vosk_model.load(model_path)
        else :
            return None #skip next line
        progress_bar.stop()

    def __set_vosk_engine(self, model) :
        """ Set the vosk engine, and vosk output for usage :
        model : the infos model to configure in vosk engine.
        output : the method use to out sentence for vosk engine.
        path : the file path in case of output in file."""
        #set vosk engine and output
        self.vosk_engine = simple_vosk.SimpleVosk(self.vosk_model)
        if self['vosk_output'] == 0 : #case keyboard
            self.vosk_output = simple_vosk.VoskKeyboard()
            #self.vosk_engine.set_filter("all")
            self.vosk_engine.set_post_process(f=self.vosk_output.input_sentence)
        elif self['vosk_output'] == 1 : #case file text
            label = self.interface.get_object("vosk_realtime_label")
            self.vosk_output = simple_vosk.VoskFile(self['vosk_file_target'], printing=label.set_text)
            #self.vosk_engine.set_filter("text")
            self.vosk_engine.set_post_process(f=self.vosk_output.write)
        elif self['vosk_output'] == 2 : #case of TextView
            self.vosk_output = VoskTextView(self)
            #self.vosk_engine.set_filter("all")
            self.vosk_engine.set_post_process(f=self.vosk_output.input)

    def __vosk_verify_options(self) :
        """ Checks the consistency of the interface options.
        Return True if ok or False to stop starting transcription."""
        if self["vosk_model"] == "" :
            self["vosk_message"] = self.messages['error_no_model']
            return False
        if self["vosk_output"] == 1 and self['vosk_file_target'] is None :
            self["vosk_message"] = self.messages['error_outfile']
            return False
        self[f"vosk_message"] = ""
        return True

    def vosk_start_transcription(self, *args) :
        """ Start transcription and other run others module if necessary."""
        #if error in interface options : stop
        if not self.__vosk_verify_options() : return None
        #change widget
        self.__widget_hide("vosk_start_transcript", False)
        self.__widget_hide("vosk_stop_transcript", True)
        self.__set_window_priority(
            priority=[1, 2, 0][self['window_priority']] #[1, 2, 0] is the order of option in glade interface
        )
        #set model if necessary
        model_infos = self.vosk_models.get_available()[self['vosk_model']]
        model_path = f"{self.vosk_models.models_path}/{model_infos['name']}"
        self.__set_vosk_model(model_path)
        #set vosk engine and vosk output
        self.__set_vosk_engine(model_infos)
        #start transcription
        if self['vosk_output'] == 0 : #start keyboard
            self.vosk_output.start()
        self.vosk_engine.start()

    def vosk_stop_transcription(self, *args) :
        """ Stop transcription and start recasepunc if necessary."""
        #stop transcription
        if self['vosk_output'] == 0 :  #stop keyboard
            self.vosk_output.stop()
        self.vosk_engine.stop()
        self.vosk_output = None
        #change widget
        self.__widget_hide("vosk_start_transcript", True)
        self.__widget_hide("vosk_stop_transcript", False)
        self.__set_window_priority()
        #run recasepunc
        if self['add_punctuation'] :
            print("Add ponctuation !")

    def add_model(self, *args) :
        """ Start zenity sub-window to download a modele."""
        new_models = self.vosk_models.ask_for_new_model(
            self.vosk_models.get_unavailable()
        )
        if not new_models : return None
        # install a model
        self.vosk_models.add_models(self.vosk_models.get_infos(new_models))
        self.__set_available_lang()

    def remove_model(self, *args) :
        """ Start zenity sub-window to download a modele."""
        selected = self['vosk_model_list']
        print("selected :", selected)
        self.vosk_models.remove_model(self['vosk_model_list'])
        self.__set_available_lang()

    def manage_recasepunc(self, *args) :
        recasepunc_mm = model_manager.ModelManager("recasepunc")
        new_conf = recasepunc_mm.ask_for_new_model(recasepunc_mm.models_infos)
        if not new_conf : return None
        #upgrade models
        recasepunc_mm.upgrade_model(recasepunc_mm.get_infos(new_conf))

    def copy_text_zone(self, *args) :
        """ Copy text from the GtkTextViewer."""
        cb = Gtk.Clipboard.get(SELECTION_CLIPBOARD)
        cb.set_text(self['textbuffer'], -1)
        cb.store()

    def erase_text_zone(self, *args) :
        """ Erase the totality of text zone."""
        self['textbuffer'] = ""



class VoskTextView :
    def __init__(self, parent) :
        self.__parent = parent

    def __insert_text(self, text) :
        text_buffer = self.__parent.interface.get_object('textbuffer')
        text_buffer.insert_at_cursor(text)
        self.__insert_partial("")

    def __insert_partial(self, text) :
        self.__parent['vosk_realtime_label'] = text

    def input(self, sentence) :
        if "text" in sentence :
            if sentence['text'] == "" : return None
            self.__insert_text(sentence['text'] + " ")
        else :
            if sentence['partial'] == "" : return None
            self.__insert_partial(sentence['partial'])



if __name__ == "__main__" :
    vb = VoskBoard()
    Gtk.main()
