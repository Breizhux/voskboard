#!/usr/bin/env python3

import os
import json
import shutil
from subprocess import Popen, PIPE

SCRIPT_DOWNLOAD_MODEL = """while read -d $'\\r' ligne; do
    valeur="${ligne##* }"
    echo "${valeur%,*}" | grep -v "#"
done < <(curl --progress-bar --anyauth --create-dirs --output-dir "$dir$" -O "$url$" 2>&1) | zenity \
    --progress --width 300 --no-cancel \
    --title="Téléchargement en cours..." \
    --text="$name$" \
    --auto-close --auto-kill
"""
SCRIPT_DOWNLOAD_NONZIP_FILE = """wget -q -O \"$path$\" $url$ 2>&1 | zenity \
    --progress --pulsate --width 300 --no-cancel \
    --title="Téléchargement en cours..." \
    --text="$name$" \
    --auto-close --auto-kill
"""
SCRIPT_UNZIP_MODEL = """unzip -o "$filename$" -d "$model_dir$" | zenity \
    --progress --pulsate --width 300 --no-cancel \
    --title="Extraction de l'archive..." \
    --text="Dézippage de l'archive en cours..." \
    --auto-close --auto-kill
"""

def system_command(command, cut=None) :
    """ Run system command, return (exit code, output)."""
    process = Popen(command, stdout=PIPE, stderr=PIPE,
                    shell=True, encoding='utf-8', executable="/bin/bash")
    output = process.stdout.read().strip(" ,:.'\"\n")
    while process.poll() is None : pass
    return (process.poll(), output)

def zenity_download(directory, url, name) :
    """ Download a file with zenity progress bar."""
    #prepare bash script
    if url.endswith(".zip") :
        script = SCRIPT_DOWNLOAD_MODEL.replace(
            "$dir$", directory).replace("$url$", url).replace("$name$", name)
    else :
        script = SCRIPT_DOWNLOAD_NONZIP_FILE.replace(
            "$path$", f"{directory}/{name}").replace("$url$", url).replace("$name$", name)
    #run and verify error
    out = system_command(script)[0]
    if out != 0 : #error
        command = "zenity --error --width 300 --title=\"Erreur téléchargement de modèle\""
        command += " --text=\"Une erreur est survenue lors du téléchargement du modèle."
        command += f"\n{name}\""
        system_command(command)
        return False
    return True

def zenity_unzip(directory, path, name) :
    """ Unzip a file with zenity progress bar."""
    #prepare bash script
    script = SCRIPT_UNZIP_MODEL.replace("$filename$", path).replace(
                                        "$model_dir$", directory)
    #run and verify error
    out = system_command(script)[0]
    if out != 0 : #error
        command = "zenity --error --width 300 --title=\"Erreur ajout de modèle\""
        command += " --text=\"Une erreur est survenue lors de l'extraction du modèle."
        command += f"\nname\""
        system_command(command)
        return False
    return True

def zenity_list_check(elements) :
    """ Ask what model user want download with zenity."""
    command = "zenity --list --checklist  --height=300 --width=550 "
    command += "--title=\"Choisissez des modèles\" "
    command += "--text=\"Choisissez les modèles que vous voulez télécharger :\" "
    command += '--column "" --column "Name" --column "Size" --column "Codename" '
    for i in elements :
        command += f"{i[0]} \"{i[1]}\" \"{i[2]}\" \"{i[3]}\" "
    rc, output = system_command(command)
    if rc != 0 :
        return False
    return output.split('|')

def zenity_entry(title, text) :
    command = "zenity --entry --width 425 "
    command += f"--title \"{title}\" --text \"{text}\""
    rc, output = system_command(command)
    if rc != 0 : return False
    return output

def zenity_confirm(title, text) :
    command = "zenity --question --width 300 "
    command += f'--title="{title}" '
    command += f"--text='{text}'"
    return system_command(command)[0]



class ModelManager :
    other_model_string = "Autre modèle"

    def __init__(self, model_type) :
        if model_type == "vosk" :
            self.models_path = f"{os.environ['HOME']}/.config/voskboard/vosk_models"
            if not os.path.exists(self.models_path) :
                os.makedirs(self.models_path)
            with open("datas/vosk_models_data.json", 'r') as file :
                self.models_infos = json.load(file)
        elif model_type == "recasepunc" :
            self.models_path = f"{os.environ['HOME']}/.config/voskboard/recasepunc_models"
            if not os.path.exists(self.models_path) :
                os.makedirs(self.models_path)
            with open("datas/recasepunc_models_data.json", 'r') as file :
                self.models_infos = json.load(file)

    def __iter__(self) :
        for i in self.models_infos :
            yield i

    def __getitem__(self, k) :
        if k == self.other_model_string :
            return self.add_manual_model(action="get_dict")
        for i in self.models_infos :
            if k in list(i.values()) :
                return i

    def get_infos(self, k) :
        """ Same as __getitem__, but for multiple."""
        if len(k) == 1 and k[0] == "" : return []
        return [self[i] for i in k]

    def get_available(self) :
        return self.get_infos(os.listdir(self.models_path))

    def get_unavailable(self) :
        unavailable = []
        for i in self :
            if not os.path.exists(f"{self.models_path}/{i['name']}") :
                unavailable.append(i)
        return unavailable

    def add_model(self, infos) :
        #download model file
        if not zenity_download(self.models_path, infos['url'], infos['name']) :
            return False
        #calculate path of downloaded file
        model_path = f"{self.models_path}/{os.path.basename(infos['url'])}"
        #unzip file if is a zipped model, after delete zip archive
        if model_path.endswith(".zip") :
            if not zenity_unzip(self.models_path, model_path, infos['name']) :
                return False
            os.remove(model_path)
        #else, verify if model have the correct name, else rename it.
        #elif os.path.basename(infos['url']) != infos['name'] :
        #    os.rename(model_path, f"{self.models_path}/{infos['name']}")
        return True

    def add_models(self, some) :
        for i in some : self.add_model(i)

    def ask_for_new_model(self, source) :
        """ call "zenity_list_check". source is the list
        of element to include."""
        elements = []
        for i in source :
            path = f"{self.models_path}/{i['name']}"
            first = "TRUE" if os.path.exists(path) else "FALSE"
            elements.append((first, i['human'], i['size'], i['name']))
        return zenity_list_check(elements)

    def add_manual_model(self, action="install") :
        """ Zenity ask, download, unzip manuel from direct link.
        Possible action : install/get_dict"""
        #ask the url
        text = "Vous pouvez trouver de nouveaux modèles ici :\n"
        text += "https://alphacephei.com/vosk/models\n\n"
        text += "Veuillez indiquer l'url de téléchargement :"
        output = zenity_entry("Ajouter un nouveau modèle", text)
        #create infos dictionnary
        infos = { "name" : os.path.basename(os.path.splitext(output)[0]),
                  "human" : "Modèle manuel",
                  "url" : output }
        #install new model or just return dict
        if action == "install" :
            return self.add_model(infos)
        else :
            return infos

    def remove_model(self, model) :
        """ Remove a model with zenity confirmation."""
        if not isinstance(model, dict) : model = self[model]
        text = f"Voulez-vous vraiment supprimer ce modèle :\n\"{model['human']}\""
        if zenity_confirm("Confirmez la suppression", text) == 0 :
            path = f"{self.models_path}/{model['name']}"
            if os.path.isfile(path) : os.remove(path)
            else : shutil.rmtree(path)

    def upgrade_model(self, conf) :
        """ Verify and upgrade (delete/download) models comparing of conf."""
        #deleting models
        for i in self.get_available() :
            if not i in conf :
                self.remove_model(i)
        #add new model if necessary
        available = self.get_available()
        for i in conf :
            if not i in available :
                self.add_model(i)

if __name__ == "__main__" :
    import sys

    test = "vosk"

    if test == "vosk" :
        vosk_mm = ModelManager("vosk")

        # ask in list of model
        a = vosk_mm.ask_for_new_model(vosk_mm.get_unavailable())
        if not a : sys.exit()
        print(vosk_mm.get_infos(a))
        # install a model
        vosk_mm.add_model(vosk_mm[a[0]])
        # remove a model
        vosk_mm.remove_model(vosk_mm[a[0]])

    elif test == "recasepunc" :
        rcpunc_mm = ModelManager("recasepunc")

        #ask in list of model
        b = rcpunc_mm.ask_for_new_model(rcpunc_mm.models_infos)
        if not b : sys.exit()
        print(rcpunc_mm.get_infos(b))
        # upgrade models to current
        rcpunc_mm.upgrade_model(rcpunc_mm.get_infos(b))
