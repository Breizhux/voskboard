#!/bin/bash

initial_dir="$PWD"
project_dir="voskboard/usr/share/voskboard"

dir_to_deb() {
    source_dir="$1"
    destination="$2"

    if [ -e "$destination" ]; then
        rm "$destination"
    fi

    chmod -R 755 "$source_dir/DEBIAN/"

    dpkg-deb --build "$source_dir/"
    mv "$source_dir.deb" "$destination"
    chmod 755 "$destination"
}

error() {
    echo "An error is occured."
    echo "$1"
    exit 2
}

need_apt_dependency() {
    echo "To construct package, the script need \"virtualenv\" and \"python Gtk\" packages."
    echo "You can resolve with : sudo apt install -y virtualenv python3-gi"
    echo "Please, install it and rerun this script."
    exit 1
}

#create virtualenv
cd "$project_dir"
virtualenv ./ || need_apt_dependency
source bin/activate

#install dependency
pip install -r requirements.txt
python_version=$(ls lib/ | grep python3*)
if [ ! -e ./lib/$python_version/site-packages/gi ]; then
    ln -s /usr/lib/python3/dist-packages/gi ./lib/$python_version/site-packages/ || need_apt_dependency
fi

#create .deb file
cd "$initial_dir"
dir_to_deb voskboard voskboard.deb || error "Error when generate debian package."

if [ -e "voskboard.deb" ]; then
    echo ""
    echo "Debian package generated !"
    echo ""
    echo "Run this command to terminate installation :"
    echo "sudo apt install ./voskboard.deb"
fi
